// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import frc.robot.Constants;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

/** Subsystem which controls the climber mechanism. */
public final class ClimberSubsystem extends SubsystemBase {

  private final CANSparkMax climberMotor;
  private static ClimberSubsystem instance;
  private boolean climberEnable;
  private double speed;

  /** Subsystem which controls the climber mechanism. */
  private ClimberSubsystem() {
    climberMotor = new CANSparkMax(Constants.Climber.MOTOR_ID, MotorType.kBrushless);
    climberMotor.set(0);

    climberEnable = false;
  }

  public void toggleClimber() {
    climberEnable = !climberEnable;
  }

  public void setSpeed(double speed) {
    this.speed = speed;
  }

  @Override
  public void periodic() {
    if (climberEnable) {
      climberMotor.set(speed);
    } else {
      climberMotor.set(Constants.Climber.STOPPED_SPEED);
    }
  }

  /** Ensures that only one instance of the subsystem exists at a time.
  *
  * @return the {@link ClimberSubsystem} singleton
  */
  public static ClimberSubsystem getInstance() {
    if (instance == null) {
      instance = new ClimberSubsystem();
      CommandScheduler.getInstance().registerSubsystem(instance);
    }
    return instance;
  }
}
