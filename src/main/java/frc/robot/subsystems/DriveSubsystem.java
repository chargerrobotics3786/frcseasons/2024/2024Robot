// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import frc.robot.Constants;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.drive.DifferentialDrive.WheelSpeeds;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

/** Subsystem for controlling a differential drive train. */
public final class DriveSubsystem extends SubsystemBase {

  private static DriveSubsystem instance;
  // all drive base motors
  private final CANSparkMax leftFrontMotor = new CANSparkMax(
        Constants.DriveTrain.LEFT_FRONT_MOTOR_ID, MotorType.kBrushless);
  private final CANSparkMax leftBackMotor = new CANSparkMax(
        Constants.DriveTrain.LEFT_BACK_MOTOR_ID, MotorType.kBrushless);
  private final CANSparkMax rightFrontMotor = new CANSparkMax(
        Constants.DriveTrain.RIGHT_FRONT_MOTOR_ID, MotorType.kBrushless);
  private final CANSparkMax rightBackMotor = new CANSparkMax(
        Constants.DriveTrain.RIGHT_BACK_MOTOR_ID, MotorType.kBrushless);

  // not sure if these should set a variable then have it set the motor speed in periodic or not.
  // if there is a situation where having a variable for the speeds is beneficial, I will change it.
  // consumers for the motor speeds

  private DifferentialDrive differentialDrive;

  private DriveSubsystem() {
    differentialDrive = new DifferentialDrive(
        (x) -> {
          leftFrontMotor.set(x);
          leftBackMotor.set(x);
        },
        (x) -> {
          rightFrontMotor.set(x);
          rightBackMotor.set(x);
        }
    );
    // the idle mode is so that the robot stops when ever there are no inputs
    /* the ramp rate is set to open loop due to the disuse of PID, ->
    and less complexity compared to the latter option.
    the ramp rate is set on the MotorController to free up computational power on the RoboRIO
    */
    leftFrontMotor.setIdleMode(CANSparkMax.IdleMode.kCoast);
    leftFrontMotor.setInverted(Constants.DriveTrain.LEFT_FRONT_INVERTED);
    leftFrontMotor.setOpenLoopRampRate(Constants.DriveTrain.RAMP_RATE_SECONDS);
    leftBackMotor.setIdleMode(CANSparkMax.IdleMode.kCoast);
    leftBackMotor.setInverted(Constants.DriveTrain.LEFT_BACK_INVERTED);
    leftBackMotor.setOpenLoopRampRate(Constants.DriveTrain.RAMP_RATE_SECONDS);
    rightFrontMotor.setIdleMode(CANSparkMax.IdleMode.kCoast);
    rightFrontMotor.setInverted(Constants.DriveTrain.RIGHT_FRONT_INVERTED);
    rightFrontMotor.setOpenLoopRampRate(Constants.DriveTrain.RAMP_RATE_SECONDS);
    rightBackMotor.setIdleMode(CANSparkMax.IdleMode.kCoast);
    rightBackMotor.setInverted(Constants.DriveTrain.RIGHT_BACK_INVERTED);
    rightBackMotor.setOpenLoopRampRate(Constants.DriveTrain.RAMP_RATE_SECONDS);
  }

  /** Ensures that only one instance of {@link DriveSubsystem} exisists at a time.
   *
   * @return A singleton of {@link DriveSubsystem}.
   */
  public static DriveSubsystem getInstance() {
    if (instance == null) {
      instance = new DriveSubsystem();
      CommandScheduler.getInstance().registerSubsystem(instance);
    }
    return instance;
  }

  /** Uses the differentialDrive object to move the drive train in tank drive.
   *
   * @param leftSpeed a speed for the left side of the drive train. Range of [-1, 1].
   * @param rightSpeed a speed for the right side of the drive train. Range of [-1, 1].
   * @param squareInputs if the inputs will be squared, more sensitive at higher inputs.
  */
  public void tankDrive(double leftSpeed, double rightSpeed, boolean squareInputs) {
    differentialDrive.tankDrive(leftSpeed, rightSpeed, squareInputs);
  }

  /**
   * Gets the drive train speeds by creating a new object with the motors set speeds.
   *
   * @return both of the speeds being in the range of [-1,1].
   */
  public WheelSpeeds getWheelSpeeds() {
    return new WheelSpeeds(leftFrontMotor.get(), rightFrontMotor.get());
  }

  /** Gets the drive train speeds for use in odometry.
   * Gets it by returning a new object with the motors' set speeds.
   *
   * @return both of the speeds being in the range of [-1,1].
  */
  public DifferentialDriveWheelSpeeds getDiffWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(leftFrontMotor.get(), rightFrontMotor.get());
  }
}