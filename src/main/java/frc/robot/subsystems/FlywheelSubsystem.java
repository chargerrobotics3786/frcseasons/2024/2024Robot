// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkLowLevel.MotorType;
import frc.robot.Constants;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.networktables.DoubleEntry;
import edu.wpi.first.networktables.DoublePublisher;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;

/** Subsystem for the flywheel on the shooter. */
public final class FlywheelSubsystem extends SubsystemBase {

  private final CANSparkMax flywheelMotor;
  private final NetworkTable flywheelTable;
  public DoubleEntry flywheelSpeedEntry;
  private static FlywheelSubsystem instance;
  private double flywheelSpeed;
  private boolean isEntryEnabled;
  private RelativeEncoder flywheelEncoder;
  private DoublePublisher flywheelSpeedPublisher;
  private DoublePublisher flywheelEncoderPublisher;

  /** Subsystem for control and network table reading of flywheel motor. */
  private FlywheelSubsystem() {
    flywheelMotor =
        new CANSparkMax(Constants.Flywheel.MOTOR_ID, MotorType.kBrushless);
    flywheelMotor.setInverted(Constants.Flywheel.MOTOR_INVERTED);
    flywheelMotor.setIdleMode(CANSparkMax.IdleMode.kCoast);
    flywheelSpeed = Constants.Flywheel.STOPPED_SPEED;
    flywheelTable =
        NetworkTableInstance.getDefault().getTable(Constants.Flywheel.NetworkTables.TABLE_NAME);
    isEntryEnabled = false;
    flywheelEncoder = flywheelMotor.getEncoder();
    flywheelEncoder.setVelocityConversionFactor(Constants.Flywheel.VELOCITY_CONVERSION_FACTOR);
    flywheelSpeedPublisher = flywheelTable
            .getDoubleTopic(Constants.Flywheel.NetworkTables.PUBLISHER_SPEED_NAME).publish();
    flywheelEncoderPublisher = flywheelTable
            .getDoubleTopic(Constants.Flywheel.NetworkTables.ENCODER_NAME).publish();
  }

  @Override
  public void periodic() {
    flywheelMotor.set(flywheelSpeed);
    flywheelSpeedPublisher.set(flywheelSpeed);
    flywheelEncoderPublisher.set(flywheelEncoder.getVelocity());
  }

  /** Ensures that only one instance of the subsystem exists at a time.
  *
  * @return the {@link FlywheelSubsystem} singleton
  */
  public static FlywheelSubsystem getInstance() {
    if (instance == null) {
      instance = new FlywheelSubsystem();
      CommandScheduler.getInstance().registerSubsystem(instance);
    }
    return instance;
  }

  /** Method to initialize the NetworkTable Entry for the flywheel's speed.
  *
  * @apiNote should only be ran inside {@link frc.robot.Robot#testInit()}
  */
  public void flywheelEntryInitialize() {
    flywheelSpeedEntry = flywheelTable
        .getDoubleTopic(Constants.Flywheel.NetworkTables.ENTRY_SPEED_NAME)
            .getEntry(flywheelSpeed);
    flywheelSpeedEntry.set(flywheelSpeed);
    isEntryEnabled = true;
    flywheelSpeed = Constants.Flywheel.STOPPED_SPEED;
  }

  /** Method to set the flywheel entry to 0, and disable the motor if not already.
   *
   * @apiNote should only be ran inside {@link frc.robot.Robot#teleopInit()} */
  public void flywheelEntryClose() {
    flywheelSpeed = Constants.Flywheel.STOPPED_SPEED;
    if (isEntryEnabled) {
      flywheelSpeedEntry.unpublish();
      isEntryEnabled = false;
    }
  }

  /** Checks if the flywheel has reached the speed it's being set to. */
  public boolean isFlywheelAtSpeed() {
    return MathUtil.isNear(flywheelSpeed,
      flywheelEncoder.getVelocity(),
      Constants.Flywheel.SPEED_TOLERANCE);
  }

  /** Sets the speed of the flywheel.
   *
   * @param speed the speed of the motor
  */
  public void setSpeed(double speed) {
    flywheelSpeed = speed;
  }

}