// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import edu.wpi.first.networktables.DoubleEntry;
import edu.wpi.first.networktables.DoublePublisher;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

/** Subsystem for the kicker motor(s), and methods to control kicker motor(s). */
public final class KickerSubsystem extends SubsystemBase {
  //Vars for kicker motor
  private final CANSparkMax kickerMotor;
  private final NetworkTable kickerTable;
  private final DoublePublisher kickerSpeedPub;
  private final DoublePublisher kickerEncoderPublisher;
  private static KickerSubsystem instance;
  private double kickerSpeed;
  public DoubleEntry kickerSpeedEntry;
  private boolean isEntryEnabled;

  /** Creates a new KickerSubsystem. */
  private KickerSubsystem() {
    //Create New Kicker Motor
    kickerMotor =
        new CANSparkMax(Constants.Kicker.MOTOR_ID, MotorType.kBrushless);

    //Set Kicker Motor Settings
    kickerMotor.setInverted(Constants.Kicker.MOTOR_INVERTED);
    kickerMotor.setIdleMode(CANSparkMax.IdleMode.kCoast);
    kickerMotor.getEncoder()
        .setVelocityConversionFactor(Constants.Kicker.VELOCITY_CONVERSION_FACTOR);

    //Kicker NetworkTable
    kickerTable =
        NetworkTableInstance.getDefault().getTable(Constants.Kicker.NetworkTables.TABLE_NAME);

    //Kicker Publishers
    kickerSpeedPub =
        kickerTable.getDoubleTopic(Constants.Kicker.NetworkTables.PUBLISHER_SPEED_NAME).publish();
    kickerEncoderPublisher =
        kickerTable.getDoubleTopic(Constants.Kicker.NetworkTables.ENCODER_NAME).publish();

    //Kicker speed
    kickerSpeed = Constants.Kicker.STOPPED_SPEED;

    isEntryEnabled = false;
  }

  @Override
  public void periodic() {
    kickerSpeedPub.set(kickerSpeed);
    kickerEncoderPublisher.set(kickerMotor.getEncoder().getVelocity());
    kickerMotor.set(kickerSpeed);
  }

  /** Sets the speed of the kicker. */
  public void setSpeed(double speed) {
    kickerSpeed = speed;
  }

  /** Ensures that only one instance of the subsystem exists at a time.
  *
  * @return the {@link KickerSubsystem} singleton
  */
  public static KickerSubsystem getInstance() {
    if (instance == null) {
      instance = new KickerSubsystem();
      CommandScheduler.getInstance().registerSubsystem(instance);
    }
    return instance;
  }

  /** Method to initialize the NetworkTable Entry for the kicker's speed.
  *
  * @apiNote should only be ran inside {@link frc.robot.Robot#testInit()}
  */
  public void kickerEntryInitialize() {
    kickerSpeedEntry = kickerTable
        .getDoubleTopic(Constants.Flywheel.NetworkTables.ENTRY_SPEED_NAME)
            .getEntry(kickerSpeed);
    kickerSpeedEntry.set(kickerSpeed);
    isEntryEnabled = true;
    kickerSpeed = Constants.Flywheel.STOPPED_SPEED;
  }

  /** Method to set the kicker entry to 0, and disable the motor if not already.
   *
   * @apiNote should only be ran inside {@link frc.robot.Robot#teleopInit()} */
  public void kickerEntryClose() {
    kickerSpeed = Constants.Flywheel.STOPPED_SPEED;
    if (isEntryEnabled) {
      kickerSpeedEntry.unpublish();
      isEntryEnabled = false;
    }
  }
}
