// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.networktables.BooleanPublisher;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.networktables.DoubleEntry;
import edu.wpi.first.networktables.DoublePublisher;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.math.filter.Debouncer;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import frc.robot.Constants;

/** Subsystem which controls the groundIntake mechanism. */
public final class GroundIntakeSubsystem extends SubsystemBase {
  private static GroundIntakeSubsystem instance;
  private final CANSparkMax groundIntakeMotor;
  private final NetworkTable intakeTable;
  private DoubleEntry groundIntakeSpeedEntry;
  private final BooleanPublisher hasNoteStatePublisher;
  private final DigitalInput breakSensorInput;
  private final DoubleSolenoid intakeSolenoid;
  private double speed;
  private boolean isEntryEnabled;
  private boolean hasNote;
  private final DoublePublisher groundIntakeSpeedPublisher;
  private final DoublePublisher groundIntakeEncoderPublisher;
  /** This debouncer is to get rid of any short changes in the state like a small object going
   * through the collector or a short vibration. */
  private final Debouncer breakSensorDebouncer;

  /** Ensures that only one instance of the subsystem exists at a time.
  *
  * @return the {@link GroundIntakeSubsystem} singleton
  */
  public static GroundIntakeSubsystem getInstance() {
    if (instance == null) {
      instance = new GroundIntakeSubsystem();
      CommandScheduler.getInstance().registerSubsystem(instance);
    }
    return instance;
  }

  /** Subsystem that controls the ground intake motor. */
  private GroundIntakeSubsystem() {
    groundIntakeMotor = new CANSparkMax(Constants.GroundIntake.MOTOR_PORT, MotorType.kBrushless);
    groundIntakeMotor.setIdleMode(IdleMode.kCoast);
    breakSensorInput = new DigitalInput(Constants.GroundIntake.IR_BEAMBREAK_SENSOR_DIO_PORT);
    breakSensorDebouncer = new Debouncer(Constants.GroundIntake.DEBOUNCE_TIME_SECONDS,
        Debouncer.DebounceType.kBoth);
    hasNote = !breakSensorInput.get(); // initial value
    intakeSolenoid = new DoubleSolenoid(
      Constants.GroundIntake.Pneumatics.HUB_ID,
      PneumaticsModuleType.REVPH,
      Constants.GroundIntake.Pneumatics.INTAKE_RAISE_ID,
      Constants.GroundIntake.Pneumatics.INTAKE_LOWER_ID);
    intakeSolenoid.set(DoubleSolenoid.Value.kForward);

    intakeTable =
        NetworkTableInstance.getDefault()
            .getTable(Constants.GroundIntake.NetworkTables.TABLE_NAME);
    hasNoteStatePublisher = intakeTable.getBooleanTopic(
      Constants.GroundIntake.NetworkTables.HAS_NOTE_STATE_NAME).publish();
    isEntryEnabled = false;

    groundIntakeMotor.getEncoder()
        .setVelocityConversionFactor(Constants.GroundIntake.VELOCITY_CONVERSION_FACTOR);
    groundIntakeEncoderPublisher = intakeTable
        .getDoubleTopic(Constants.GroundIntake.NetworkTables.ENCODER_NAME).publish();
    groundIntakeSpeedPublisher = intakeTable
        .getDoubleTopic(Constants.GroundIntake.NetworkTables.PUBLISHER_SPEED_NAME).publish();
  }

  /** Toggles ground intake motor with inputed speed.
   *
   * @param speed represents speed to set motor at
  */
  public void intakeEnable(double speed) {
    this.speed = speed;
  }

  /** Gets the note state of the collector.
   *
   * @return true if a note is in the collector, false if there is no note.
   */
  public boolean hasNote() {
    return hasNote;
  }

  /** Uses the BeamBreak sensor in the DIO port for detecting if a note is in the system.
   *
   * @return true if the beam is unbroken, false if the beam is broken
   */
  private boolean isBeamUnbroken() {
    return breakSensorDebouncer.calculate(breakSensorInput.get());
  }

  private void updateHasNoteState() {
    hasNote = !isBeamUnbroken();
  }

  /** Gets the ground intake speed entry object.
   *
   * @return groundIntakeSpeedEntry
   *
  */
  public DoubleEntry getGroundIntakeSpeedEntry() {
    return groundIntakeSpeedEntry;
  }

  public void setSolenoidDirection(DoubleSolenoid.Value solenoidDirection) {
    intakeSolenoid.set(solenoidDirection);
  }

  @Override
  public void periodic() {
    groundIntakeMotor.set(speed);
    updateHasNoteState();
    hasNoteStatePublisher.set(hasNote());
    groundIntakeEncoderPublisher.set(groundIntakeMotor.getEncoder().getVelocity());
    groundIntakeSpeedPublisher.set(speed);
  }

  /** Method to initialize the NetworkTable Entry for the collector's speed.
   *
   * @apiNote only should be ran inside {@link frc.robot.Robot#testInit()}
  */
  public void groundIntakeEntryInitialize() {
    groundIntakeSpeedEntry = intakeTable
        .getDoubleTopic(Constants.GroundIntake.NetworkTables.ENTRY_SPEED_NAME).getEntry(speed);
    groundIntakeSpeedEntry.set(speed);
    isEntryEnabled = true;
  }

  /** Method to set the groundIntake entry to 0, and disable the motor if not already.
   *
   *@apiNote only should be ran inside {@link frc.robot.Robot#teleopInit()}*/
  public void groundIntakeEntryClose() {
    if (isEntryEnabled) {
      groundIntakeSpeedEntry.unpublish();
      isEntryEnabled = false;
    }
  }
}


