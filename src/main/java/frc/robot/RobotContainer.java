// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.MathUtil;
import java.util.Map;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.NetworkTableValue;
import edu.wpi.first.networktables.StringEntry;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.StartEndCommand;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import edu.wpi.first.wpilibj2.command.SelectCommand;
import frc.robot.subsystems.FlywheelSubsystem;
import frc.robot.commands.collector.GroundIntakeCollectCommand;
import frc.robot.commands.collector.GroundIntakeEjectCommand;
import frc.robot.commands.drive.TankDrive;
import frc.robot.commands.flywheel.FlywheelSpeedCommand;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.KickerSubsystem;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.subsystems.GroundIntakeSubsystem;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and trigger mappings) should be declared here.
 */
public class RobotContainer {
  // Subsystems
  private final GroundIntakeSubsystem groundIntakeSubsystem = GroundIntakeSubsystem.getInstance();
  private final DriveSubsystem driveSubsystem = DriveSubsystem.getInstance();
  private final FlywheelSubsystem flywheelSubsystem = FlywheelSubsystem.getInstance();
  private final KickerSubsystem kickerSubsystem = KickerSubsystem.getInstance();

  // Controllers
  private final CommandXboxController driverController =
      new CommandXboxController(Constants.Driver.CONTROLLER_PORT);
  private final CommandXboxController operatorController =
      new CommandXboxController(Constants.Operator.CONTROLLER_PORT);

  // Commands
  private final Command flywheelTestCommand =
      new StartEndCommand(() -> flywheelSubsystem.setSpeed(
        MathUtil.clamp(flywheelSubsystem.flywheelSpeedEntry.getAsDouble(),
          Constants.Flywheel.MINIMUM_SPEED,
              Constants.Flywheel.MAXIMUM_SPEED)),
          () -> flywheelSubsystem.setSpeed(Constants.Flywheel.STOPPED_SPEED),
      flywheelSubsystem);
  private final Command kickerTestCommand =
      new StartEndCommand(() -> kickerSubsystem.setSpeed(
        MathUtil.clamp(kickerSubsystem.kickerSpeedEntry.getAsDouble(),
          Constants.Kicker.MINIMUM_SPEED,
              Constants.Kicker.MAXIMUM_SPEED)),
          () -> kickerSubsystem.setSpeed(Constants.Kicker.STOPPED_SPEED),
      kickerSubsystem);
  private final Command groundIntakeTestCommand =
      new StartEndCommand(() -> groundIntakeSubsystem.intakeEnable(
        MathUtil.clamp(groundIntakeSubsystem.getGroundIntakeSpeedEntry().getAsDouble(),
          Constants.GroundIntake.MINIMUM_SPEED,
              Constants.GroundIntake.MAXIMUM_SPEED)),
          () -> groundIntakeSubsystem.intakeEnable(Constants.GroundIntake.STOPPED_SPEED),
      groundIntakeSubsystem);
  private final InstantCommand groundIntakeDeployCommand =
      new InstantCommand(() -> groundIntakeSubsystem.setSolenoidDirection(Value.kReverse),
          groundIntakeSubsystem);
  private final InstantCommand groundIntakeRetractCommand =
      new InstantCommand(() -> groundIntakeSubsystem.setSolenoidDirection(Value.kForward),
          groundIntakeSubsystem);
  private final GroundIntakeCollectCommand groundIntakeCollectCommand =
      new GroundIntakeCollectCommand(groundIntakeSubsystem,
      Constants.GroundIntake.COLLECTING_MOTOR_SPEED);
  private final Command ejectCommandGroup =
      new InstantCommand(
          () -> groundIntakeSubsystem.intakeEnable(Constants.GroundIntake.MOTOR_EJECT_SPEED))
      .andThen(() -> kickerSubsystem.setSpeed(Constants.Kicker.MOTOR_EJECT_SPEED))
      .andThen(new WaitCommand(Constants.GroundIntake.EJECT_TIME_SECONDS))
      .finallyDo(() -> {
        kickerSubsystem.setSpeed(Constants.Kicker.STOPPED_SPEED);
        groundIntakeSubsystem.intakeEnable(Constants.GroundIntake.STOPPED_SPEED);
      });
  private final Command groundIntakeCollectCommandGroup =
      new InstantCommand(() -> groundIntakeSubsystem.setSolenoidDirection(Value.kReverse),
          groundIntakeSubsystem)
      .andThen(new GroundIntakeCollectCommand(groundIntakeSubsystem,
          Constants.GroundIntake.COLLECTING_MOTOR_SPEED))
      .finallyDo(() -> groundIntakeSubsystem.setSolenoidDirection(Value.kForward));
  private final Command groundIntakeEjectCommandGroup =
      new InstantCommand(() -> groundIntakeSubsystem.setSolenoidDirection(Value.kReverse),
          groundIntakeSubsystem)
      .andThen(new GroundIntakeEjectCommand(groundIntakeSubsystem,
          Constants.GroundIntake.MOTOR_EJECT_SPEED))
      .finallyDo(() -> groundIntakeSubsystem.setSolenoidDirection(Value.kForward));
  private final TankDrive tankDrive = new TankDrive(driveSubsystem,
      // negative because the y on the controllers are positive is down, negative is up.
      // first lamda controls the left side, second lamda controls the right side.
      () ->  -driverController.getLeftY(), () -> -driverController.getRightY(),
      // third lamda is for slow mode.
      () -> driverController.rightBumper().getAsBoolean(),
      // fourth lamda is for fast mode.
      () -> driverController.leftBumper().getAsBoolean(),
      () -> driverController.rightTrigger().getAsBoolean());
  private final Command speakerShotCommandGroup =
      new FlywheelSpeedCommand(flywheelSubsystem, Constants.Flywheel.SPEAKER_SHOT_MOTOR_SPEED)
        .andThen(() -> groundIntakeSubsystem.intakeEnable(
          Constants.GroundIntake.SPEAKER_SHOT_MOTOR_SPEED), groundIntakeSubsystem)
        .andThen(() -> kickerSubsystem.setSpeed(Constants.Kicker.SPEAKER_SHOT_MOTOR_SPEED),
          kickerSubsystem)
        .andThen(new WaitUntilCommand(() -> !groundIntakeSubsystem.hasNote()))
        .andThen(new WaitCommand(Constants.Flywheel.SPEAKER_FEED_TIME_SECONDS))
        .finallyDo(() -> {
          kickerSubsystem.setSpeed(Constants.Kicker.STOPPED_SPEED);
          groundIntakeSubsystem.intakeEnable(Constants.GroundIntake.STOPPED_SPEED);
          flywheelSubsystem.setSpeed(Constants.Flywheel.STOPPED_SPEED);
        });
  private final Command simpleDriveForwardAuto =
      new RunCommand(() ->
          driveSubsystem.tankDrive(Constants.Autonomous.SimpleDriveForward.SPEED_PERCENTAGE,
          Constants.Autonomous.SimpleDriveForward.SPEED_PERCENTAGE, false), driveSubsystem)
      .withTimeout(Constants.Autonomous.SimpleDriveForward.DRIVE_TIME_SECONDS)
      .andThen(() -> driveSubsystem.tankDrive(
          Constants.DriveTrain.STOPPED_SPEED,
          Constants.DriveTrain.STOPPED_SPEED,
          false),
          driveSubsystem);
  private final Command indexDriveOut =
      new RunCommand(() ->
          driveSubsystem.tankDrive(
              Constants.DriveTrain.INDEX_DRIVE_OUT_SPEED,
              Constants.DriveTrain.INDEX_DRIVE_OUT_SPEED,
              false),
              driveSubsystem)
      .withTimeout(Constants.DriveTrain.INDEX_DRIVE_OUT_SECONDS)
      .finallyDo(() -> driveSubsystem.tankDrive(
          Constants.DriveTrain.STOPPED_SPEED,
          Constants.DriveTrain.STOPPED_SPEED,
          false));
  private final Command autoShootCommandGroup =
      new FlywheelSpeedCommand(flywheelSubsystem, Constants.Flywheel.SPEAKER_SHOT_MOTOR_SPEED)
        .andThen(() -> groundIntakeSubsystem.intakeEnable(
          Constants.GroundIntake.SPEAKER_SHOT_MOTOR_SPEED), groundIntakeSubsystem)
        .andThen(() -> kickerSubsystem.setSpeed(Constants.Kicker.SPEAKER_SHOT_MOTOR_SPEED),
          kickerSubsystem)
        .andThen(new WaitUntilCommand(() -> !groundIntakeSubsystem.hasNote()))
        .andThen(new WaitCommand(Constants.Flywheel.SPEAKER_FEED_TIME_SECONDS))
        .finallyDo(() -> {
          kickerSubsystem.setSpeed(Constants.Kicker.STOPPED_SPEED);
          groundIntakeSubsystem.intakeEnable(Constants.GroundIntake.STOPPED_SPEED);
          flywheelSubsystem.setSpeed(Constants.Flywheel.STOPPED_SPEED);
        });
  private final Command ampShotCommandGroup =
      new FlywheelSpeedCommand(flywheelSubsystem, Constants.Flywheel.AMP_SHOT_MOTOR_SPEED)
        .andThen(() -> groundIntakeSubsystem.intakeEnable(
          Constants.GroundIntake.AMP_SHOT_MOTOR_SPEED), groundIntakeSubsystem)
        .andThen(() -> kickerSubsystem.setSpeed(Constants.Kicker.AMP_SHOT_MOTOR_SPEED),
          kickerSubsystem)
        .andThen(new WaitUntilCommand(() -> !groundIntakeSubsystem.hasNote()))
        .andThen(new WaitCommand(Constants.Flywheel.AMP_FEED_TIME_SECONDS))
        .finallyDo(() -> {
          kickerSubsystem.setSpeed(Constants.Kicker.STOPPED_SPEED);
          groundIntakeSubsystem.intakeEnable(Constants.GroundIntake.STOPPED_SPEED);
          flywheelSubsystem.setSpeed(Constants.Flywheel.STOPPED_SPEED);
        });
  private final Command shootDriveOutAuto =
        new FlywheelSpeedCommand(flywheelSubsystem, Constants.Flywheel.SPEAKER_SHOT_MOTOR_SPEED)
        .andThen(() -> groundIntakeSubsystem.intakeEnable(
          Constants.GroundIntake.SPEAKER_SHOT_MOTOR_SPEED), groundIntakeSubsystem)
        .andThen(() -> kickerSubsystem.setSpeed(Constants.Kicker.SPEAKER_SHOT_MOTOR_SPEED),
          kickerSubsystem)
        .andThen(new WaitUntilCommand(() -> !groundIntakeSubsystem.hasNote()))
        .andThen(new WaitCommand(Constants.Flywheel.SPEAKER_FEED_TIME_SECONDS))
        .andThen(new RunCommand(() ->
          driveSubsystem.tankDrive(Constants.Autonomous.SimpleDriveForward.SPEED_PERCENTAGE,
          Constants.Autonomous.SimpleDriveForward.SPEED_PERCENTAGE, false), driveSubsystem)
      .withTimeout(Constants.Autonomous.SimpleDriveForward.DRIVE_TIME_SECONDS))
      .andThen(() -> driveSubsystem.tankDrive(
          Constants.DriveTrain.STOPPED_SPEED,
          Constants.DriveTrain.STOPPED_SPEED,
          false),
          driveSubsystem)
        .finallyDo(() -> {
          kickerSubsystem.setSpeed(Constants.Kicker.STOPPED_SPEED);
          groundIntakeSubsystem.intakeEnable(Constants.GroundIntake.STOPPED_SPEED);
          flywheelSubsystem.setSpeed(Constants.Flywheel.STOPPED_SPEED);
        });

  // auto chooser
  private final NetworkTable autoNetworkTable = NetworkTableInstance.getDefault()
      .getTable("Autonomous");
  private final StringEntry autoNameEntry =
      autoNetworkTable.getStringTopic("Auto Selector").getEntry(Constants.Autonomous.autoNames[0]);
  private final Map<String, Command> autoMap = Map.of(
      Constants.Autonomous.autoNames[0], simpleDriveForwardAuto,
      Constants.Autonomous.autoNames[1], new InstantCommand(),
      Constants.Autonomous.autoNames[2], autoShootCommandGroup,
      Constants.Autonomous.autoNames[3], shootDriveOutAuto);
  private final SelectCommand<String> autoSelectCommand = new SelectCommand<>(
      autoMap, () -> autoNameEntry.get());

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    // Sets the values a second time because they won't show up without setting values.
    autoNameEntry.set(Constants.Autonomous.autoNames[0]);
    autoNetworkTable.putValue("Auto Names",
        NetworkTableValue.makeStringArray(Constants.Autonomous.autoNames));
    // Configure the trigger bindings
    configureBindings();
  }

  /** Sets different button bindings for test and teleop modes. */
  public void initBindings(boolean isTest) {
    unbindBindings();
    configureBindings();
    if (isTest) {
      configureTestBindings();
    } else {
      configureTeleopBindings();
    }
  }

  private void configureBindings() {
    // Driver controller bindings
    driveSubsystem.setDefaultCommand(tankDrive);
    driverController.a().toggleOnTrue(indexDriveOut);
  }

  /** Defines controller mappings that are exclusive to teleop mode. */
  private void configureTeleopBindings() {
    operatorController.a().toggleOnTrue(speakerShotCommandGroup);
    operatorController.x().toggleOnTrue(groundIntakeCollectCommand);
    operatorController.b().toggleOnTrue(ejectCommandGroup);
    operatorController.y().toggleOnTrue(ampShotCommandGroup);
  }

  /** Defines controller mappings that are exclusive to test mode. */
  private void configureTestBindings() {
    operatorController.a().whileTrue(flywheelTestCommand);
    operatorController.y().whileTrue(kickerTestCommand);
    operatorController.x().toggleOnTrue(groundIntakeTestCommand);
    operatorController.rightBumper().onTrue(groundIntakeRetractCommand);
    operatorController.leftBumper().onTrue(groundIntakeDeployCommand);
  }

  /** clears all controller bindings. */
  private void unbindBindings() {
    CommandScheduler.getInstance().getActiveButtonLoop().clear();
  }

  /** Gets the selected Autonomous command.  */
  public Command getAutonomousCommand() {
    return autoSelectCommand;
  }
}
