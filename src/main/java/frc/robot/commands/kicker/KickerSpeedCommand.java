// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.kicker;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.KickerSubsystem;

/** Creates a command which sets the speed of the kicker motor. */
public class KickerSpeedCommand extends Command {

  private final KickerSubsystem kickerSubsystem;
  private final double speed;

  /** Creates a command which sets the speed of the kicker motor.
   *
   * @param kickerSubsystem refers to the subsystem required for the command
  */
  public KickerSpeedCommand(KickerSubsystem kickerSubsystem, double speed) {
    addRequirements(kickerSubsystem);
    this.kickerSubsystem = kickerSubsystem;
    this.speed = speed;
  }

  @Override
  public void initialize() {
    kickerSubsystem.setSpeed(speed);
  }

  @Override
  public void end(boolean interrupted) {
    kickerSubsystem.setSpeed(0);
  }
}
