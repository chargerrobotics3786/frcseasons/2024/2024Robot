// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drive;

import java.util.function.DoubleSupplier;
import java.util.function.BooleanSupplier;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.math.MathUtil;
import frc.robot.Constants;
import frc.robot.subsystems.DriveSubsystem;

/**
 * A command that uses the DriveSubsystem to drive the robot when executed.
 * Applies dead bands to the supplied values.
 * Does not finish, should be used as a default command.
 */
public class TankDrive extends Command {

  private DriveSubsystem driveSubsystem;

  private DoubleSupplier leftSpeed;
  private DoubleSupplier rightSpeed;
  private BooleanSupplier slowMode;
  private BooleanSupplier fastMode;
  private BooleanSupplier reversedMode;

  /** Creates a new Tank drive object. The suppliers are for the speed
   * of each repsective side and should be in the range of [-1, 1]
   *
   * @param leftSpeed a double supplier which can be a lamda expression:
   *        () -> (a double value or expression),
   *        value should be in the range of [-1, 1]
   * @param rightSpeed a double supplier which can be a lamda expression:
   *        () -> (a double value or expression),
   *        value should be in the range of [-1, 1]
   * @param slowMode a boolean supplier which can be a lamda expression:
   *        () -> (a boolean value or expression),
   *        true decreases the inputs.
   * @param fastMode a boolean supplier which can be a lamda expression:
   *        () -> (a boolean value or expression),
   *        true increases the inputs.
   * @param reversed a boolean supplier which can be a lamda expression:
   *        () -> (a boolean value or expression),
   *        true reverses the robot controls so the front is now the shooter side,
   *        normally the front is the collector side,
  */
  public TankDrive(DriveSubsystem driveSubsystem, DoubleSupplier leftSpeed,
                    DoubleSupplier rightSpeed, BooleanSupplier slowMode,
                    BooleanSupplier fastMode, BooleanSupplier reversed) {
    this.driveSubsystem = driveSubsystem;

    this.slowMode = slowMode;
    this.fastMode = fastMode;
    this.leftSpeed = leftSpeed;
    this.rightSpeed = rightSpeed;
    this.reversedMode = reversed;

    addRequirements(driveSubsystem);
  }

  /** Called every time the scheduler runs while the command is scheduled. */
  public void execute() {
    double leftSpd;
    double rightSpd;
    if (!reversedMode.getAsBoolean()) {
      leftSpd = leftSpeed.getAsDouble();
      rightSpd = rightSpeed.getAsDouble();
    } else {
      leftSpd = -rightSpeed.getAsDouble();
      rightSpd = -leftSpeed.getAsDouble();
    }

    leftSpd = MathUtil.applyDeadband(leftSpd, Constants.Driver.DEADBAND);
    rightSpd = MathUtil.applyDeadband(rightSpd, Constants.Driver.DEADBAND);

    if (slowMode.getAsBoolean() == fastMode.getAsBoolean()) {
      leftSpd *= Constants.DriveTrain.DEFAULT_SPEED_MULTIPLIER;
      rightSpd *= Constants.DriveTrain.DEFAULT_SPEED_MULTIPLIER;
    } else if (slowMode.getAsBoolean()) {
      leftSpd *= Constants.DriveTrain.SLOW_MODE_MULTIPLIER;
      rightSpd *= Constants.DriveTrain.SLOW_MODE_MULTIPLIER;
    } else {
      leftSpd *= Constants.DriveTrain.FAST_MODE_MULTIPLIER;
      rightSpd *= Constants.DriveTrain.FAST_MODE_MULTIPLIER;
    }

    driveSubsystem.tankDrive(leftSpd, rightSpd, true);
  }
}
