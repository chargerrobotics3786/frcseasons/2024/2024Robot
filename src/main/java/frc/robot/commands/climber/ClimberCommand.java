// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.climber;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ClimberSubsystem;

/** Command that turns on and off the climber motor. */
public class ClimberCommand extends Command {
  private final ClimberSubsystem climberSubsystem;
  private final double motorSpeed;

  /** Command that turns on and off the climber motor.
   *
   * @param climberSubsystem the subsystem used by this command
   * @param motorSpeed speed for the climber motor
  */
  public ClimberCommand(ClimberSubsystem climberSubsystem, double motorSpeed) {
    this.motorSpeed = motorSpeed;
    this.climberSubsystem = climberSubsystem;
    addRequirements(climberSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    climberSubsystem.setSpeed(motorSpeed);
    climberSubsystem.toggleClimber();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    climberSubsystem.toggleClimber();
  }
}
