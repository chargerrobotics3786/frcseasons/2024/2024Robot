// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.collector;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.GroundIntakeSubsystem;

/** Command that turns on and off the ground intake motor to spit out a note. */
public class GroundIntakeEjectCommand extends Command {
  private final GroundIntakeSubsystem groundIntakeSubsystem;
  private final double motorSpeed;

  /** Command that turns on and off the ground intake motor.
   *
   * @param groundIntakeSubsystem the subsystem used by this command
   * @param motorSpeed the speed of the motor as a percentage [-1, 0]
  */
  public GroundIntakeEjectCommand(
      GroundIntakeSubsystem groundIntakeSubsystem,
      double motorSpeed) {
    this.groundIntakeSubsystem = groundIntakeSubsystem;
    this.motorSpeed = motorSpeed;
    addRequirements(groundIntakeSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    groundIntakeSubsystem.intakeEnable(motorSpeed);
  }

  /** Called periodically to see if it should finish.
   *
   * @return true if the beam is unbroken while the motor is pushing a note out.
   */
  @Override
  public boolean isFinished() {
    return !groundIntakeSubsystem.hasNote();
  }
}
