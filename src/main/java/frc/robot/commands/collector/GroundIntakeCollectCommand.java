// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.collector;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.GroundIntakeSubsystem;

/** Command that turns on and off the ground intake motor to collect a note. */
public class GroundIntakeCollectCommand extends Command {
  private final GroundIntakeSubsystem groundIntakeSubsystem;
  private final double motorSpeed;

  /** Command that turns on and off the ground intake motor.
   *
   * @param groundIntakeSubsystem the subsystem used by this command
   * @param motorSpeed the speed of the motor as a percentage [0, 1]
  */
  public GroundIntakeCollectCommand(
      GroundIntakeSubsystem groundIntakeSubsystem,
      double motorSpeed) {
    this.groundIntakeSubsystem = groundIntakeSubsystem;
    this.motorSpeed = motorSpeed;
    addRequirements(groundIntakeSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    groundIntakeSubsystem.intakeEnable(motorSpeed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    groundIntakeSubsystem.intakeEnable(0);
  }

  /** Called periodically to see if it should finish.
   *
   * @return true if the beam is broken while the motor is intaking
   */
  @Override
  public boolean isFinished() {
    return groundIntakeSubsystem.hasNote();
  }
}
