// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.flywheel;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.FlywheelSubsystem;

/** Command to toggle if the flywheel motor is spinning or not. */
public class FlywheelSpeedCommand extends Command {

  private final FlywheelSubsystem flywheelSubsystem;
  private final double speed;

  /** Command to toggle if the flywheel motor is spinning or not.
   *
   * @param flywheelSubsystem refers to the subsystem required for this command
  */
  public FlywheelSpeedCommand(FlywheelSubsystem flywheelSubsystem, double speed) {
    addRequirements(flywheelSubsystem);
    this.flywheelSubsystem = flywheelSubsystem;
    this.speed = speed;
  }

  @Override
  public void initialize() {
    flywheelSubsystem.setSpeed(speed);
  }

  @Override
  public boolean isFinished() {
    return flywheelSubsystem.isFlywheelAtSpeed();
  }
}
