// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

  /** Constants used for the driver controller. */
  public static class Driver {
    public static final int CONTROLLER_PORT = 0;
    public static final double DEADBAND = 0.1;
  }

  /** Constants for the Operator. */
  public static class Operator {
    public static final int CONTROLLER_PORT = 1;
  }

  /** Constants used for the Climber. */
  public static class Climber {
    public static final int MOTOR_ID = 31;
    public static final double MOTOR_SPEED_EXTEND = -0.1;
    public static final double MOTOR_SPEED_RETRACT = 0.1;
    public static final double STOPPED_SPEED = 0;
  }

  /** Constants used for the drive train. */
  public static class DriveTrain {
    public static final int LEFT_FRONT_MOTOR_ID = 2;
    public static final int LEFT_BACK_MOTOR_ID = 3;
    public static final int RIGHT_FRONT_MOTOR_ID = 4;
    public static final int RIGHT_BACK_MOTOR_ID = 5;
    public static final boolean LEFT_FRONT_INVERTED = true;
    public static final boolean LEFT_BACK_INVERTED = true;
    public static final boolean RIGHT_FRONT_INVERTED = false;
    public static final boolean RIGHT_BACK_INVERTED = false;

    public static final double SLOW_MODE_MULTIPLIER = 0.4;
    public static final double DEFAULT_SPEED_MULTIPLIER = 0.8;
    public static final double FAST_MODE_MULTIPLIER = 1.0;
    public static final double STOPPED_SPEED = 0.0;
    public static final double RAMP_RATE_SECONDS = 0.2;

    public static final double INDEX_DRIVE_OUT_SECONDS = 1.0;
    public static final double INDEX_DRIVE_OUT_SPEED = 0.1;
  }

  /** Constants used to control the ground intake. */
  public static class GroundIntake {
    public static final int MOTOR_PORT = 21;
    public static final double COLLECTING_MOTOR_SPEED = 0.3;
    public static final double SPEAKER_SHOT_MOTOR_SPEED = 1.0;
    public static final double MOTOR_EJECT_SPEED = -0.2;
    public static final double AMP_SHOT_MOTOR_SPEED = 0.1;
    public static final double STOPPED_SPEED = 0.0;
    public static final double MINIMUM_SPEED = -1;
    public static final double MAXIMUM_SPEED = 1;
    public static final int IR_BEAMBREAK_SENSOR_DIO_PORT = 0;
    public static final double DEBOUNCE_TIME_SECONDS = 0.2;
    public static final double EJECT_TIME_SECONDS = 2.0;

    /** The max rpm of the neo is 5676.
     * the rpm needs to be divided by 56.76 to find the speed.
     * it then needs to be divided by 100 to find the percentage. */
    public static final double VELOCITY_CONVERSION_FACTOR = 1 / 56.76 / 100;

    /** Constants for the ground intake network table. */
    public static class NetworkTables {
      public static final String TABLE_NAME = "Ground Intake";
      public static final String PUBLISHER_SPEED_NAME = "Ground Intake Motor Speed % Setpoint";
      public static final String ENTRY_SPEED_NAME
          = "Set Ground Intake Motor Speed (Test Mode Only)";
      public static final String ENCODER_NAME = "Ground Intake Motor Actual Speed %";
      public static final String HAS_NOTE_STATE_NAME = "Has Note State";
    }

    /** Constants used for the pneumatics. */
    public static class Pneumatics {
      public static final int HUB_ID = 41;

      public static final int INTAKE_RAISE_ID = 0;
      public static final int INTAKE_LOWER_ID = 1;
    }
  }

  /** Constants for the flywheel subsystem. */
  public static class Flywheel {
    //Motor CAN ID's
    public static final int MOTOR_ID = 11;
    //Wait values for shooter command groups
    public static final double SPEAKER_FEED_TIME_SECONDS = 1.5;
    public static final double AMP_FEED_TIME_SECONDS = 3.0;
    //Motor Speeds
    public static final double SPEED_TOLERANCE = 0.03;
    public static final double SPEAKER_SHOT_MOTOR_SPEED = 1.0;
    public static final double AMP_SHOT_MOTOR_SPEED = 0.1;
    public static final double STOPPED_SPEED = 0;
    public static final double MINIMUM_SPEED = -1;
    public static final double MAXIMUM_SPEED = 1;
    //Motor Inverted Values
    public static final boolean MOTOR_INVERTED = false;

    /** The max rpm of the neo is 5676.
     * the rpm needs to be divided by 56.76 to find the speed.
     * it then needs to be divided by 100 to find the percentage. */
    public static final double VELOCITY_CONVERSION_FACTOR = 1 / 56.76 / 100;


    /** Constants for the flywheel NetworkTable. */
    public static class NetworkTables {
      public static final String TABLE_NAME = "Flywheel Motor Info";
      public static final String PUBLISHER_SPEED_NAME = "Flywheel Motor Speed % Setpoint";
      public static final String ENTRY_SPEED_NAME = "Set Flywheel Motor Speed (Test Mode Only)";
      public static final String ENCODER_NAME = "Flywheel Motor Actual Speed %";
    }
  }

  /** Constants used for the kicker subsystem. */
  public static class Kicker {

    /** Constants for the kicker NetworkTable. */
    public static class NetworkTables {
      public static final String TABLE_NAME = "Kicker Motor(s) Info";
      public static final String PUBLISHER_SPEED_NAME = "Kicker Motor Speed % Setpoint";
      public static final String ENTRY_SPEED_NAME = "Set Kicker Motor Speed (Test Mode Only)";
      public static final String ENCODER_NAME = "Kicker Motor Actual Speed %";
    }

    //Motor CAN ID's
    public static final int MOTOR_ID = 12;
    //Motor Speed
    public static final double STOPPED_SPEED = 0;
    public static final double SPEAKER_SHOT_MOTOR_SPEED = 1.0;
    public static final double AMP_SHOT_MOTOR_SPEED = 0.1;
    public static final double MOTOR_EJECT_SPEED = -0.2;
    public static final double MINIMUM_SPEED = -1;
    public static final double MAXIMUM_SPEED = 1;
    //Motor Inverted Values
    public static final boolean MOTOR_INVERTED = false;

    /** The max rpm of the neo is 5676.
     * the rpm needs to be divided by 56.76 to find the speed.
     * it then needs to be divided by 100 to find the percentage. */
    public static final double VELOCITY_CONVERSION_FACTOR = 1 / 56.76 / 100;
  }

  /** Constants used in autonomous commands. */
  public static class Autonomous {
    public static String[] autoNames = {"Simple", "Nothing", "Shoot", "ShootDrive"};

    /** Constants for the SimpleDriveForward auto. */
    public static class SimpleDriveForward {
      public static final double SPEED_PERCENTAGE = 0.4;
      public static final double DRIVE_TIME_SECONDS = 2.5;
    }
  }
}
